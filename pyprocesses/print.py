'''
This program prints the number of D*+ and D*0 mesons and their properties.
'''

import ROOT
from ROOT import TFile, TChain, TClonesArray, gSystem
from ROOT import ExRootTreeReader, GenParticle, TLorentzVector
from array import *

def printMesons(fin):
    
    gSystem.Load("libDelphes")
    # Initialize counters
    pcount, gluon, quarks, higgs, Kmeson, pion, neutprot, leptons, ZBoson, photon, diquarks, lcount = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    countelec, countmu, ccount, cbarcount = 0, 0, 0, 0
    
    # Output file to store the results
    fout = TFile("/home/taherburhani/Softwares/madgraph2.9/firstanalysis/resultstcbar/print.root", "RECREATE")
    fout.cd()
    
    # Create chain of root trees
    chain = TChain("Delphes")
    chain.Add(fin)
    
    # Create object of class ExRootTreeReader
    tree = ExRootTreeReader(chain)
    nEntries = tree.GetEntries()
    
    # Print the number of entries
    print("There are", nEntries, "events in my ntuple")
    
    # Point to the relevant branches
    bparticle = tree.UseBranch("Particle")
    
    # Loop over each entry in the tree
    for entry in range(nEntries):
        if entry != 0:
            print("Reading Event", entry)
        
        tree.ReadEntry(entry)
        
        print("Index", "PID", "\tStatus", "M1", "M2", "\t", "D1", "\t", "D2", "\t\t", "PT", "\t\t", "Eta", "\t\t", "Phi", "\t\t", "Mass", " ")
        
        # Loop for the partons
        dp, dn = [], []
        
        for i in range(bparticle.GetEntries()):
            particle = ROOT.GenParticle(bparticle.At(i))
            pid, status, m1, m2, d1, d2 = particle.PID, particle.Status, particle.M1, particle.M2, particle.D1, particle.D2
            pt, eta, phi, mass = particle.PT, particle.Eta, particle.Phi, particle.Mass
            pcount += 1
            
            if pid == -423:
                print(i, pid, status, m1, m2, d1, d2, pt, "\t", eta, "\t", phi, "\t", mass)
                
            elif pid == -413:
                print(i, pid, status, m1, m2, d1, d2, pt, "\t", eta, "\t", phi, "\t", mass)
                    
        print(" \n particles in the event", bparticle.GetEntries())
    
    print("loop", lcount, "entries", pcount, "gluons", gluon, "pions", pion, "Kmesons", Kmeson, "neutrons&protons", neutprot, "leptons", leptons, "higgs", higgs, "quarks", quarks, "diquarks", diquarks, "photon", photon, "ZBoson", ZBoson, "subt", pcount - gluon - neutprot - pion - Kmeson - leptons - higgs - quarks)

